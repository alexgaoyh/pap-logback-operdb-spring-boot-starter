package com.pap.logback.operdb.bean;

import com.pap.logback.operdb.filter.LogBackOperDBFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;

public class LogBackOperDBFilterRegistrationBean extends FilterRegistrationBean<LogBackOperDBFilter> {

    public LogBackOperDBFilterRegistrationBean() {
        super();
        this.setFilter(new LogBackOperDBFilter());
        this.addUrlPatterns("/*");
        this.setName("LogBackOperDBFilter");
        this.setOrder(1);
    }

}