package com.pap.logback.operdb.interceptor;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Auther: alexgaoyh
 * @Date: 2019/3/4 14:10
 * @Description:
 */
public class LogBackOperDBInterceptor extends HandlerInterceptorAdapter {

    private static Logger dbLogger = LoggerFactory.getLogger("DB_LOGGER");

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HandlerMethod handlerMethod = (HandlerMethod) handler;
        // 获取类上注解
        Class<?> clazz = handlerMethod.getBeanType();
        if (clazz.isAnnotationPresent(Api.class)) {
            Api api = (Api) clazz.getAnnotation(Api.class);
            // 获取方法上的注解
            ApiOperation apiOperation = handlerMethod.getMethodAnnotation(ApiOperation.class);
            dbLogger.info("{}{}{}", "JWT获取用户", api.value(), apiOperation.value());
        }

        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView)
            throws Exception {
        // TODO Auto-generated method stub
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
            throws Exception {
        // TODO Auto-generated method stub
    }

}
