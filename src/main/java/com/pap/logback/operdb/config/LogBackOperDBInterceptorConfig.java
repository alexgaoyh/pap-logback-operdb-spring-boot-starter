package com.pap.logback.operdb.config;

import com.pap.logback.operdb.interceptor.LogBackOperDBInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @Auther: alexgaoyh
 * @Date: 2019/3/4 14:13
 * @Description:
 */
public class LogBackOperDBInterceptorConfig implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new LogBackOperDBInterceptor());
    }

}
