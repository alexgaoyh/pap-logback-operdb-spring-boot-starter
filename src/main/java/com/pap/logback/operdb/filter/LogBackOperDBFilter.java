package com.pap.logback.operdb.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class LogBackOperDBFilter implements Filter {

    private static Logger dbLogger = LoggerFactory.getLogger("DB_LOGGER");

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // dbLogger.info("logFilter init...");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        //  从request中获取到访问的地址，并在控制台中打印出来
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        String papToken = request.getHeader("papToken");
        // token解析为对象(用户信息)

        dbLogger.info("{}{}{}{}", "1", "2", "3" ,"4");
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {
        // dbLogger.info("logFilter destroy...");
    }
}