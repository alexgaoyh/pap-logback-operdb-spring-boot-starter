package com.pap.logback.operdb;

import com.pap.logback.operdb.bean.LogBackOperDBFilterRegistrationBean;
import com.pap.logback.operdb.config.LogBackOperDBInterceptorConfig;
import com.pap.logback.operdb.filter.LogBackOperDBFilter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
// @ConditionalOnClass({LogBackOperDBFilterRegistrationBean.class, LogBackOperDBFilter.class})
@ConditionalOnClass({LogBackOperDBInterceptorConfig.class})
public class LogBackOperDBAutoConfiguration {

//    @Bean
//    @ConditionalOnMissingBean(LogBackOperDBFilterRegistrationBean.class)
//    public LogBackOperDBFilterRegistrationBean logFilterRegistrationBean() {
//        return new LogBackOperDBFilterRegistrationBean();
//    }

    /**
     * 修改为 拦截器
     * @return
     */
    @Bean
    @ConditionalOnMissingBean(LogBackOperDBInterceptorConfig.class)
    public LogBackOperDBInterceptorConfig logBackOperDBInterceptorConfig() {
        return new LogBackOperDBInterceptorConfig();
    }

}
